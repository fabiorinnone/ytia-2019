import java.util.ArrayList;

public class EsempioWarnings {

    public static void main(String[] args) {

        aggiungiElementiAdUnaLista();
        faiQualcosa(1);
        faiQualcosa(2);
        faiQualcosa(3);
        faiQualcosa(4);

        System.out.println(dividiPerZero(5));
    }

    private static double dividiPerZero(int n) {
        return n / 0;
    }

    @SuppressWarnings("unsafe")
    private static void aggiungiElementiAdUnaLista() {
        ArrayList<String> laMiaLista = new ArrayList<>();
        laMiaLista.add("Pippo");
        laMiaLista.add("Peppe");
    }

    //@SuppressWarnings("fallthrough")
    private static void faiQualcosa(int i) {
        switch (i) {
            case 1:
                System.out.println("Uno");
                //break;
            case 2:
                System.out.println("Due");
                //break;
            case 3:
                System.out.println("Tre");
                //break;
            default:
                System.out.println("Boh");
        }
    }
}
