import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class LockThread implements Runnable {
    private int numeroIntero;
    private Lock lock;

    public LockThread(int numeroIntero) {
        this.numeroIntero = numeroIntero;
        this.lock = new ReentrantLock();

        new Thread(this).start();
    }

    public void run() {
        try {
            if (lock.tryLock(10, TimeUnit.SECONDS)) {
                System.out.println("In lock: " + numeroIntero);
                numeroIntero++;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            lock.unlock();
        }

        System.out.println("Out lock: " + numeroIntero);
    }
}

public class LockExample {

    public static void main(String[] args) {
        new LockThread(1);
    }
}
