public class Circonferenza extends Figura {

    int r;

    public Circonferenza(int x, int y, int r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    @Override
    public double area() {
        return Math.PI * r * r;
    }

    @Override
    public String toString() {
        return "La circonferenza ha centro nelle coordinate " +
                "(" + x + "," + y + ") e raggio: " + r;
    }
}
