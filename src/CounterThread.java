public class CounterThread extends Thread {

    public void run() {
        for (int i = 0; i <= 10; i++)
            System.out.println(i);
    }

    public static void main (String[] args) {
        CounterThread t = new CounterThread();
        t.start();

        //Thread thread = new Thread(new CounterThread());
        //thread.start();
    }
}
