public class Quadrato extends Figura {

    int l;

    public Quadrato(int x, int y, int l) {
        this.x = x;
        this.y = y;
        this.l = l;
    }

    @Override
    public double area() {
        return l * l;
    }

    @Override
    public String toString() {
        return "Il quadrato ha vertice nelle coordinate " +
                "(" + x + "," + y + ") e lato: " + l;
    }
}
