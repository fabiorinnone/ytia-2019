class Foo {

    @Deprecated
    public void pippo() {
        System.out.println("Ciao, Pippo!");
    }

    public void peppe() {
        System.out.println("Ciao, Peppe!");
    }

    public void pappo() {
        System.out.println("Ciao, Pappo!");
    }

}

public class EsempioDeprecated {

    @SuppressWarnings("deprecation")
    public static void main(String[] args) {

        Foo f = new Foo();

        f.pippo();
        f.peppe();
    }
}