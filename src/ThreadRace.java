class Clicker implements Runnable {
    private long click;
    private Thread t;
    private volatile boolean running;

    public Clicker(int priority, String nome) {
        t = new Thread(this, nome);
        t.setPriority(priority);
        click = 0;
    }

    public long getClick() {
        return click;
    }

    @Override
    public void run() {
        while (running) {
            click++;
        }
    }

    public void stopThread() {
        running = false;
    }

    public void startThread() {
        running = true;
        t.start();
    }
}

public class ThreadRace {

    //private static int MILLIS = 5000;

    public static void main(String[] args) {
        Thread ct = Thread.currentThread();
        ct.setPriority(Thread.MAX_PRIORITY);

        Clicker hi = new Clicker(7, "high");
        Clicker lo = new Clicker(3, "low");

        hi.startThread();
        lo.startThread();

        try {
            Thread.sleep(5000);
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }

        hi.stopThread();
        lo.stopThread();

        System.out.println(
                "hi: " +hi.getClick() +
                "\nlo: " + lo.getClick());
    }
}
