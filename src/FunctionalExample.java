public class FunctionalExample {

    public static void main(String[] args) {

        MiaClasse c = new MiaClasse();
    }
}

@FunctionalInterface
interface MiaInterfacciaFunzionale {

    void pippo();
    //public abstract void peppe();

    /*void foo() {
        System.out.println("Foo");
    }*/

}

class MiaClasse implements MiaInterfacciaFunzionale {

    @Override
    public void pippo() {

    }

    /*@Override
    public void peppe() {

    }*/
}