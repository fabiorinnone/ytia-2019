class Magazzino {
    private int numeroProdotti;
    private int idUltimoProdotto;
    private boolean magazzinoVuoto; //il magazzino è inizialmente vuoto

    private static Magazzino instance;

    public static Magazzino getInstance() {
        if (instance == null)
            instance = new Magazzino();
        return instance;
    }

    private Magazzino() {
        numeroProdotti = 0;
        magazzinoVuoto = true;
    }

    /*public synchronized static Magazzino getInstance() {
        if (instance == null)
            return new Magazzino();
        return instance;
    }*/

    public synchronized void aggiungi(int idProdotto) {
        if (!magazzinoVuoto) { //se il magazzino non è vuoto
            try {
                wait(); //il produttore si deve fermare
            }
            catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.idUltimoProdotto = idProdotto;
        numeroProdotti++;
        stampaRiepilogo("Aggiunto prodotto: " + idUltimoProdotto);
        magazzinoVuoto = false; //il magazzino non è più vuoto
        notify(); //sveglia il consumatore
    }

    public synchronized int elimina() {
        if (magazzinoVuoto) { //se il magazzino è vuoto
            try {
                wait(); //il consumatore si deve fermare
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        numeroProdotti--;
        stampaRiepilogo("Eliminato prodotto");
        magazzinoVuoto = true; //il magazzino adesso è vuoto
        notify(); // sveglia il produttore
        return idUltimoProdotto;
    }

    private void stampaRiepilogo(String messaggio) {
        System.out.println(messaggio + "\nNumero prodotti nel magazzino: " +
                numeroProdotti);
    }
}

class Produttore implements Runnable {
    private Magazzino magazzino;

    public Produttore() {
        this.magazzino = Magazzino.getInstance();
        new Thread(this, "Produttore")
                .start();
    }

    public void run() {
        for (int i = 1; i <= 10; i++) {
            magazzino.aggiungi(i);
        }
    }
}

class Consumatore implements Runnable {
    private Magazzino magazzino;

    public Consumatore() {
        this.magazzino = Magazzino.getInstance();
        new Thread(this, "Consumatore")
                .start();
    }

    public void run() {
        for (int i = 0; i < 10; i++)
            magazzino.elimina();
    }
}

public class ProduttoreConsumatore {

    public static void main(String[] args) {
        //Magazzino magazzino = Magazzino.getInstance();
        new Produttore();
        new Consumatore();
    }
}
