import java.util.ArrayList;
import java.util.Iterator;

public class EsempioListe {

    @SuppressWarnings("deprecation")
    public static void main(String[] args) {
        ArrayList<MyInteger> laMiaLista = new ArrayList<>();
        laMiaLista.add(new MyInteger(1));
        laMiaLista.add(new MyInteger(2));
        laMiaLista.add(new MyInteger(3));

        MyInteger p = new MyInteger(3);

        p.setValue(p.getValue() + 1);

        int q = p.getValue() + 1;

        String s = "pippo";
        String t = s.concat(" e peppe");
        System.out.println(t);

        /*Iterator<MyInteger> it = laMiaLista.iterator();
        while(it.hasNext()) {
            System.out.println(it.next());
        }*/

        for (MyInteger n : laMiaLista) {
            System.out.println(n);
        }
    }
}

class MyInteger extends Object {
    private int value;

    public MyInteger(int value) {
        this.value = value;
    }

    //nuova implementazione del metodo getValue()
    public int ottieniValore() {
        return value;
    }

    @Deprecated
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}