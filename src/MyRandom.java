public class MyRandom {

    /**
     * Metodo che genera numeri interi casuali in un
     * range definito dall'utente.
     * @param l Valore minimo
     * @param h Valore massimo
     * @return Un numero intero random compreso tra un valore
     * minimo e un valore massimo
     */
    public static int getRandom(int l, int h) {
        return (int)(h * Math.random() + l);
    }
}
