import java.util.concurrent.Semaphore;

class Veicolo implements Runnable {
    private Incrocio incrocio;
    private String nome;

    public Veicolo(Incrocio incrocio, String nome) {
        this.incrocio = incrocio;
        this.nome = nome;
        new Thread(this).start();
    }

    public String getNome() {
        return nome;
    }

    @Override
    public void run() {
        incrocio.attraversa(this);
    }
}

class Incrocio {

    private static Incrocio instance;

    private Incrocio () {

    }

    public static Incrocio getInstance() {
        if (instance == null) {
            instance = new Incrocio();
        }
        return instance;
    }

    Semaphore semaforo = new Semaphore(3);

    public void attraversa(Veicolo v) {

        try {
            semaforo.acquire();
            System.out.println("Il veicolo " +
                    v.getNome() + " sta attraversando l'incrocio");
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Il veicolo "
                + v.getNome() + " ha attraversato l'incrocio");
        semaforo.release();
    }
}

public class EsempioIncrocio {

    public static void main(String[] args) {
        Incrocio incrocio = Incrocio.getInstance();
        new Veicolo(incrocio,"Fiat Panda");
        new Veicolo(incrocio,"Lancia Ypsilon");
        new Veicolo(incrocio,"Ford Fiesta");
        new Veicolo(incrocio,"Fiat Tipo");
        new Veicolo(incrocio,"Toyota Yaris");
        new Veicolo(incrocio,"Renault Clio");
        new Veicolo(incrocio,"Nissan Micra");
        //incrocio.attraversa(v1);
        //incrocio.attraversa(v2);
        //incrocio.attraversa(v3);
    }
}