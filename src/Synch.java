class Stampa {
    //public static int MILLIS = 1000;

    public synchronized void stampaMessaggio(
            String messaggio) {
        System.out.print("[" + messaggio);

        try{
            Thread.sleep(5000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("]");
    }
}

class StampaThread implements Runnable {
    private Stampa s;
    private String messaggio;

    public StampaThread(Stampa s,
                        String messaggio) {
        this.s = s;
        this.messaggio = messaggio;

        new Thread(this).start();
    }

    public void run() {
        s.stampaMessaggio(messaggio);
    }
}

public class Synch {

    public static void main (String[] args) {
        Stampa s = new Stampa();
        //s.stampaMessaggio("Pippo");
        //s.stampaMessaggio("Peppe");
        //s.stampaMessaggio("Pappo");

        new StampaThread(s, "Pippo");
        new StampaThread(s, "Peppe");
        new StampaThread(s, "Pappo");
    }

}
