public class Operazioni {

    private Operazioni() {

    }

    public static String concatenaStringhe(String s1, String s2) {
        return s1 + "" + s2;
    }

    public static int sommaTraInteri(int a, int b) {
        return a + b;
    }
}
