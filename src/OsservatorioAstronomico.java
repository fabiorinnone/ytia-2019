public class OsservatorioAstronomico {

    public static void main(String[] args) {
        Telescopio telescopio = Telescopio.getInstance();

        Partecipante[] partecipanti = {
            new Partecipante("Tizio", telescopio),
            new Partecipante("Caio", telescopio),
            new Partecipante("Sempronio", telescopio)
        };

        for (int i = 0; i < partecipanti.length; i++) {
            Partecipante p = partecipanti[i];
            p.start();
        }

        for (Partecipante p : partecipanti) {
            p.start();
        }

        //new Partecipante("Mevio", telescopio);
        //new Partecipante("Filano", telescopio);
        //new Partecipante("Calpurnio", telescopio);
    }
}

class Partecipante extends Thread {

    private final String nome;
    private final Telescopio telescopio;

    public Partecipante (String nome, Telescopio telescopio) {
        this.nome = nome;
        this.telescopio = Telescopio.getInstance();
    }

    public String getNome() {
        return nome;
    }

    public void run() {
        System.out.println(nome + " sta aspettando");
        telescopio.osserva(this);
    }
}

class Telescopio {

    private static Telescopio instance = null;

    public static Telescopio getInstance() {
        if (instance == null) {
            instance = new Telescopio();
        }
        return instance;
    }

    private Telescopio() {

    }

    public synchronized void osserva(Partecipante p) {
        String nome = p.getNome();
        System.out.println(nome + " sta osservando per 12 secondi!");
        try {
            Thread.sleep(12000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(nome + " ha finito di osservare");
    }
}