public class EsempioPersona {

    public static void main(String[] args) {
        Persona p1 =
                new Persona("Pinco", "Pallino");
        Persona p2 =
                new Persona("Fabio", "Rinnone");

        Studente s =
                new Studente("Gabriele", "Guzzetta", 1243);

        System.out.println(p1.toString());
        System.out.println(p2.toString());

        System.out.println(s.toString());

        Impiegato imp = new Impiegato("Mario", "Rossi");
        System.out.println("Ho appena creato " + imp);
        imp.lavora();

        StudenteImpiegato sl =
                new StudenteImpiegato("Roberto", "Bianchi", 9999);
        sl.lavora();

        System.out.println(sl.toString());

        OperaioMetalmeccanico pippo = new OperaioMetalmeccanico();
        OperaioEdile peppe = new OperaioEdile();

        pippo.indossaTuta();
        peppe.indossaTuta();
        pippo.lavora();
        peppe.lavora();

        System.exit(0);
    }
}

class Persona {
    protected String nome;
    protected String cognome;

    public Persona(String nome, String cognome) {
        this.nome = nome;
        this.cognome = cognome;
    }

    public String getNome() {
        return nome;
    }

    public String getCognome() {
        return cognome;
    }

    @Override
    public String toString() {
        return nome + " " + cognome;
    }
}

class Studente extends Persona {
    protected int matricola;

    public Studente(String nome, String cognome, int matricola) {
        super(nome, cognome);
        this.matricola = matricola;
    }

    public int getMatricola() {
        return matricola;
    }

    public void setMatricola(int nuovaMatricola) {
        this.matricola = nuovaMatricola;
    }

    @Override
    public String toString() {
        return nome + " " +
                cognome +
                ", matricola: " + matricola;
    }
}

class Impiegato extends Persona implements Lavoratore {

    public Impiegato(String nome, String cognome) {
        super(nome, cognome);
    }

    @Override
    public String toString() {
        return "Impiegato: " + nome + " " + cognome;
    }

    public void lavora() {
        System.out.println(nome + " " + cognome + " sta lavorando...");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(nome + " " + cognome + " ha finito di lavorare");
    }
}

class StudenteImpiegato extends Studente implements Lavoratore {

    public StudenteImpiegato(String nome, String cognome, int matricola) {
        super(nome, cognome, matricola);
    }

    @Override
    public String toString() {
        return "Impiegato: " + nome +
                cognome + ", studente con matricola: " +
                matricola;
    }

    @Override
    public void lavora() {
        System.out.println(nome + " " + cognome + " sta lavorando...");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(nome + " " + cognome + " ha finito di lavorare");
    }
}

interface Lavoratore {
    public void lavora();
}

abstract class LavoratoreAstratto implements Lavoratore {

   public void timbraCartellino() {
       System.out.println("Sta timbrando il cartellino");
   }
}

class Programmatore extends LavoratoreAstratto {

    @Override
    public void lavora() {

    }
}

abstract class Operaio implements Lavoratore {

    public void indossaTuta() {
        System.out.println("Sta indossando la tuta");
    }
}

class OperaioMetalmeccanico extends Operaio implements Lavoratore {

    @Override
    public void lavora() {
        System.out.println("Sta fondendo acciaio");
    }
}

class OperaioEdile extends Operaio {

    @Override
    public void lavora() {
        System.out.println("Sta costruendo una casa");
    }
}