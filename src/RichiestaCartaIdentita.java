import java.util.Random;

public class RichiestaCartaIdentita {

    public static void main(String[] args) {
        //final Richiedente[] richiedenti = registraRichiedenti();

        new Richiedente("Tizio");
        new Richiedente("Caio");
        new Richiedente("Sempronio");
        new Richiedente("Mevio");
        new Richiedente("Filano");
        new Richiedente("Calpurnio");

        /*for (Richiedente r : richiedenti) {
            r.start();
        }*/
    }

    /*private static Richiedente[] registraRichiedenti() {
        Richiedente[] richiedenti = {
                new Richiedente("Tizio"),
                new Richiedente("Caio"),
                new Richiedente("Sempronio"),
                new Richiedente("Mevio"),
                new Richiedente("Filano"),
                new Richiedente("Calpurnio")
        };

        return richiedenti;
    }*/
}

//La classe Richiedente implementa Runnable
class Richiedente implements Runnable {
    private final String nome;

    public Richiedente(String nome) {
        this.nome = nome;
        new Thread(this).start();
    }

    public String getNome() {
        return nome;
    }

    public void run() {
        //Sportello.getInstance().gestisciRichiesta(this);
        Sportello s = Sportello.getInstance();
        s.gestisciRichiesta(this);
    }

    public String toString() {
        return nome;
    }
}

//La classe Stampante sarà usata per stampare i documenti
class Stampante {
    private static final int TEMPO_ATTESA_STAMPA = 3000;

    private static Stampante instance = null;

    private Stampante() {

    }

    public static Stampante getInstance() {
        if (instance == null)
            return new Stampante();
        return instance;
    }

    public synchronized void stampa(Richiedente richiedente) {
        System.out.println("Stampa carta di identità di " + richiedente + " in corso...");

        try {
            wait(TEMPO_ATTESA_STAMPA);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Stampa carta di identità di " + richiedente + " completata!");
    }
}

class Sportello {

    private static Sportello instance = null;

    private final Stampante stampante;

    private Sportello() {
        stampante = Stampante.getInstance();
    }

    public synchronized static Sportello getInstance() {
        if (instance == null)
            instance = new Sportello();
        return instance;
    }

    public synchronized void gestisciRichiesta(Richiedente richiedente) {
        System.out.println("Impiegato fornisce il modulo a: " + richiedente);
        compilaModulo(richiedente);
        stampante.stampa(richiedente);
    }

    private synchronized void compilaModulo(Richiedente richiedente) {
        final int tempoCompilazioneModulo = new Random().nextInt(6) + 5;

        try {
            System.out.println(richiedente + " impiegherà " + tempoCompilazioneModulo +
                    " secondi per compilare il modulo");
            wait(tempoCompilazioneModulo);
        }
        catch(InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(richiedente + " ha compilato il modulo");
    }
}