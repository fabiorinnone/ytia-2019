public class Geometria {

    public static void main(String[] args) {
        Quadrato q1 = new Quadrato(1, 2, 5);
        Quadrato q2 = new Quadrato(2, 3, 2);
        Quadrato q3 = new Quadrato(3, 5, 2);
        Circonferenza c1 = new Circonferenza(0,0,6);
        Circonferenza c2 = new Circonferenza(2,3,3);

        Figura[] figure = new Figura[5];
        figure[0] = q1;
        figure[1] = q2;
        figure[2] = q3;
        figure[3] = c1;
        figure[4] = c2;

        for (Figura f : figure) {
            System.out.println(f.toString());
        }

        for (Figura f : figure) {
            String tipoFigura = "";

            if (f instanceof Quadrato) {
                tipoFigura = "del quadrato";
            }
            else if (f instanceof Circonferenza) {
                tipoFigura = "della circonferenza";
            }
            System.out.println("L'area " + tipoFigura + " è: " + f.area());
        }

        double areaMassima = 0;
        for (Figura f : figure) {
            double area = f.area();
            if (area > areaMassima)
                areaMassima = area;
        }

        //Circonferenza[] circonferenze = new Circonferenza[2];
        //circonferenze[0] = c1;
        //circonferenze[1] = c2;

        //for (Quadrato q : quadrati) {
        //    System.out.println(q);
        //}

        /*for (Circonferenza c : circonferenze) {
            System.out.println(c);
        }*/

        /*for (int i = 0; i < quadrati.length; i++) {
            System.out.println(quadrati[i].toString());
        }*/

        System.exit(0);
    }
}
