class MyThread implements Runnable {

    public MyThread() {
        Thread ct = Thread.currentThread();
        ct.setName("Thread principale");
        Thread t = new Thread(this, "Thread figlio");

        System.out.println("Thread attuale" + ct);
        System.out.println("Thread figlio" + t);

        t.start();

        try {
            Thread.sleep(3000);

        }
        catch (InterruptedException e) {
            System.out.println("Thread principale interrotto.");
        }

        System.out.println("Uscita dal thread principale");
    }

    @Override
    public void run() {
        try {
            for (int i = 5; i >= 0; i--) {
                System.out.println("Thread figlio: " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread figlio interrotto");
        }

        System.out.println("Uscita dal thread figlio");
    }

}

public class ThreadCreation {

    public static void main(String[] args) {
        new MyThread();
    }
}
