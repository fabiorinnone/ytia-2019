class ImmutableClass {
    private final int integer; //variabile di istanza primitiva immutabile
    private final String string; //variabile di istanza complessa immutabile

    //costruttore che prende le variabili dall'esterno
    public ImmutableClass(int integer, String string) {
        this.integer = integer;
        this.string = string;
    }

    public int getInteger() {
        return integer;
    }

    public String getString() {
        return string;
    }

    public void stampaInteger() {
        System.out.println(integer);
    }

    public void stampaString() {
        System.out.println(string);
    }
}
